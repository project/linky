<?php

namespace Drupal\Tests\linky\Kernel;

use Drupal\Core\Config\Config;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\linky\Entity\Linky;
use Drupal\linky\Plugin\Validation\Constraint\LinkyLinkConstraint;

/**
 * A class to test the linky link constraint.
 *
 * @group linky
 */
class LinkyLinkConstraintTest extends LinkyKernelTestBase {

  /**
   * An email uri.
   *
   * @var string
   */
  private $emailUri = 'mailto:blah.com';

  /**
   * A telephone uri.
   *
   * @var string
   */
  private $telephoneUri = 'tel:012345';

  /**
   * An invalid uri.
   *
   * @var string
   */
  private $invalidUri = 'http:blah.com';

  /**
   * A method to get the editable config for linky settings.
   *
   * @return \Drupal\Core\Config\Config
   *   The config.
   */
  private function getEditableConfig(): Config {
    return \Drupal::configFactory()->getEditable('linky.settings');
  }

  /**
   * A method to update the linky additional schemes settings.
   *
   * @param string $setting
   *   The setting.
   * @param int $state
   *   The state.
   */
  private function updateAdditionalSchemesSetting(string $setting, int $state) {
    $this->getEditableConfig()->set("additional_schemes.{$setting}", $state)->save();
  }

  /**
   * A method to test the validation.
   */
  public function testValidation() {
    /** @var EntityFieldManagerInterface $entity_field_manager */
    $entity_field_manager = \Drupal::service('entity_field.manager');
    $this->updateAdditionalSchemesSetting('email', 1);
    $this->updateAdditionalSchemesSetting('telephone', 1);
    $entity_field_manager->clearCachedFieldDefinitions();
    $this->assertEmailAllowed();
    $this->assertTelephoneAllowed();
    $this->updateAdditionalSchemesSetting('email', 0);
    $this->updateAdditionalSchemesSetting('telephone', 0);
    $entity_field_manager->clearCachedFieldDefinitions();
    $this->assertEmailDisallowed();
    $this->assertTelephoneDisallowed();
    $this->assertInvalid();
  }

  /**
   * A method to assert an allowed email uri.
   */
  private function assertEmailAllowed() {
    $link = Linky::create([
      'link' => [
        'uri' => $this->emailUri,
      ],
    ]);
    /** @var \Drupal\Core\Entity\EntityConstraintViolationList $errors */
    $errors = $link->validate();
    $this->assertCount(0, $errors);
  }

  /**
   * A method to assert an disallowed email uri.
   */
  private function assertEmailDisallowed() {
    $link = Linky::create([
      'link' => [
        'uri' => $this->emailUri,
      ],
    ]);
    /** @var \Drupal\Core\Entity\EntityConstraintViolationList $errors */
    $errors = $link->validate();
    $this->assertCount(1, $errors);
    /** @var \Drupal\linky\Plugin\Validation\Constraint\LinkyLinkConstraint $constraint */
    $constraint = $this->prophesize(LinkyLinkConstraint::class)->reveal();
    $this->assertEquals((new TranslatableMarkup($constraint->notSupportedMessage, ['@uri' => $this->emailUri]))->__toString(), (string) $errors[0]->getMessage());
    $this->assertEquals('link.0.uri', $errors[0]->getPropertyPath());
  }

  /**
   * A method to assert an allowed telephone uri.
   */
  private function assertTelephoneAllowed() {
    $link = Linky::create([
      'link' => [
        'uri' => $this->telephoneUri,
      ],
    ]);
    /** @var \Drupal\Core\Entity\EntityConstraintViolationList $errors */
    $errors = $link->validate();
    $this->assertCount(0, $errors);
  }

  /**
   * A method to assert an disallowed telephone uri.
   */
  private function assertTelephoneDisallowed() {
    $link = Linky::create([
      'link' => [
        'uri' => $this->telephoneUri,
      ],
    ]);
    /** @var \Drupal\Core\Entity\EntityConstraintViolationList $errors */
    $errors = $link->validate();
    $this->assertCount(1, $errors);
    /** @var \Drupal\linky\Plugin\Validation\Constraint\LinkyLinkConstraint $constraint */
    $constraint = $this->prophesize(LinkyLinkConstraint::class)->reveal();
    $this->assertEquals((string) new TranslatableMarkup($constraint->notSupportedMessage, ['@uri' => $this->telephoneUri]), (string) $errors[0]->getMessage());
    $this->assertEquals('link.0.uri', $errors[0]->getPropertyPath());
  }

  /**
   * A method to assert an invalid uri.
   */
  private function assertInvalid() {
    $link = Linky::create([
      'link' => [
        'uri' => $this->invalidUri,
      ],
    ]);
    /** @var \Drupal\Core\Entity\EntityConstraintViolationList $errors */
    $errors = $link->validate();
    $this->assertCount(1, $errors);
    /** @var \Drupal\linky\Plugin\Validation\Constraint\LinkyLinkConstraint $constraint */
    $constraint = $this->prophesize(LinkyLinkConstraint::class)->reveal();
    $this->assertEquals((string) new TranslatableMarkup($constraint->invalidMessage, ['@uri' => $this->invalidUri]), (string) $errors[0]->getMessage());
    $this->assertEquals('link.0.uri', $errors[0]->getPropertyPath());
  }

}
